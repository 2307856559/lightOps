package com.gsafety.devops.service.impl;
import com.gsafety.devops.entity.DatabaseInfoEntity;
import com.gsafety.devops.service.DatabaseInfoServiceI;

import org.jeecgframework.core.common.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.io.Serializable;
import org.jeecgframework.core.util.ApplicationContextUtil;
import org.jeecgframework.core.util.MyClassLoader;
import org.jeecgframework.core.util.StringUtil;
import org.jeecgframework.web.cgform.enhance.CgformEnhanceJavaInter;

import org.jeecgframework.minidao.util.FreemarkerParseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.jeecgframework.core.util.ResourceUtil;

@Service("databaseInfoService")
@Transactional
public class DatabaseInfoServiceImpl extends CommonServiceImpl implements DatabaseInfoServiceI {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
 	public void delete(DatabaseInfoEntity entity) throws Exception{
 		super.delete(entity);
 	}
 	
 	public Serializable save(DatabaseInfoEntity entity) throws Exception{
 		Serializable t = super.save(entity);
 		return t;
 	}
 	
 	public void saveOrUpdate(DatabaseInfoEntity entity) throws Exception{
 		super.saveOrUpdate(entity);
 	}
 	
}